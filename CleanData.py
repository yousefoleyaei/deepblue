# -*- coding: utf-8 -*-
"""
Created on Wed Jul 10 12:31:03 2019

@author: Youse
2nd prep
"""
import pandas as pd
import os
from pandas import DataFrame as DF
import numpy as np
pd.options.display.width = 0
pd.set_option('display.expand_frame_repr', False)
import os 
import glob
REVISE_LS=[]

def cleaningCSV(dir_fl, fl_nm, cols, test_type, list_revise):
    
    if (test_type==0):
        nmout='train'
    else:
        nmout="test"
    out_traj_nm=nmout+'_trajectory_'+fl_nm[:-9]+".csv"              
    out_noise_nm=nmout+'_noise_'+fl_nm[:-9]+".csv"
    flnm_traj='clean_'+nmout+'_'+fl_nm[:-9]+".csv"
    df_traj=pd.read_csv(dir_fl+out_traj_nm, index_col=['idx'], \
                         usecols=cols)
    df_traj['tstep']=df_traj["ut"].copy()
    cnt_noise=df_traj.groupby('idx')['tstep'].agg({'tstep': ['count','min']})
    cnt_noise.columns=['cnt', "min_tstep"]
    df_traj_noise=df_traj.merge(cnt_noise, on=['idx'], how='left')
    df_traj_noise.sort_values(['idx', 'tstep'], inplace=True)
    df_traj_noise['tstep']=(df_traj_noise.tstep-df_traj_noise.min_tstep).copy()
    df_traj_noise.drop(['min_tstep'], inplace=True, axis=1)
#    df_traj_noise['tstep'].describe()	

    unstack_traj=df_traj_noise.copy()
    unstack_traj=unstack_traj[['latitude',  'longitude',  'altitude',  'speed', 'tstep']].set_index(
                'tstep', append=True, inplace=False).unstack(level=0)
    	
    unstack_traj=unstack_traj.reindex(np.arange( unstack_traj.index.max()+1))
    	
    	
    unstack_traj.sort_values(['tstep'], ascending=True, inplace=True)   
    	    
    interpolate_TrajNoise=unstack_traj.interpolate(method='linear', limit_direction='backward', axis=0, inplace=False)
    		
    stack_traj=interpolate_TrajNoise.stack(level=1)
    	
    stack_traj.index=stack_traj.index.swaplevel(0, 1).copy()
    	
    stack_traj.sort_index(level=[0,1], inplace=True)
    	
    	
    	
    stack_traj.reset_index(level=1, inplace=True, drop=False)
    stack_traj['ut']=df_traj_noise.groupby('idx')['ut'].min()#+stack_traj.index.get_level_values(1)
    	
    stack_traj['ut']+=stack_traj['tstep']

    df_traj_noise.reset_index(inplace=True, drop=False)
    smaller_df=df_traj_noise[['uid', 'opType', 'acCat',  'locid',  'cnt', 'idx']].groupby('idx').max()
    	
    stack_traj=stack_traj.merge(smaller_df, on=['idx'],
    	                            how='left')
    	
    	
    df_noise=pd.read_csv(dir_fl+out_noise_nm, usecols=[ 'idx', 'ut', 'db'])
    	
    join_noise=stack_traj.merge(df_noise, how='left', on=['idx','ut'])#loc[df_traj.index.isin(df_noise.index)]
    	
    stack_traj.head()
    	
    	
    df_traj_noise['b_inter']=0
    	
    joined_noise=join_noise.merge(df_traj_noise[['b_inter', 'ut', 'idx']], how='left', on= ['idx','ut'])
    	
    joined_noise['b_inter'].loc[joined_noise.b_inter!=joined_noise.b_inter]=1
    joined_noise.set_index('idx', inplace=True)
    	
    joined_noise['b_inter'].head()
    joined_noise.set_index('ut', inplace=True, drop=True, append=True)
    	
    	
    begin_end_joined_noise=joined_noise.query('locid==locid')
    	
    	
    	
    	
    df_noise_repeated=begin_end_joined_noise.copy()
    	
    	
    	
    len(df_noise_repeated.loc[~df_noise_repeated.index.duplicated(keep='first')])
    	
    	
    	
    mini_df_nan=pd.DataFrame(df_noise_repeated[['db']].reset_index(level=[0,1],
                              inplace=False,))
    mini_df_nan.drop(['ut'] , axis=1, inplace=True)
    mini_df_nan.set_index('idx', inplace=True, append=True)
    	
    	
    unstacked_mini=mini_df_nan.unstack(level=1)
    	
    unstacked_mini.columns=df_noise_repeated.index.get_level_values(0).unique()
    binary_df=unstacked_mini.isnull().all()==True
    
    rm_idx_ls=binary_df[binary_df].index
    
    df_nois_trajec=df_noise_repeated.loc[~df_noise_repeated.index.get_level_values('idx').isin(rm_idx_ls)==True]
    
    
    
    ls_invalid_uid=[50218469, 41237444, 44847572, 45000186, 47462048, 50411306, 50964370, 39721424, 41400490, 50410883]
    
    if len(df_nois_trajec.loc[df_nois_trajec.db!=df_nois_trajec.db].index.get_level_values('idx').unique())>1:
        
        print("Warning of empty noise redo please")
        
        list_revise.append(out_traj_nm)
        if len(df_nois_trajec.loc[df_nois_trajec.uid.isin(ls_invalid_uid)])>0:
            
            df_nois_trajec=df_nois_trajec.loc[~df_nois_trajec.uid.isin(ls_invalid_uid)==True]
    
    else:
        print("Success")
    		
    df_nois_trajec.loc[df_nois_trajec.db==df_nois_trajec.db].to_csv('/home/yousef/Summer19/NN/trajec_fly/CleanDATA/'+flnm_traj,
                      index=1, index_label=['idx', 'ut'], encoding='utf8')	
    	#1466827236-1466827200+1
    return list_revise


dir_fl=''

selected_features=['idx', 'ut',	'latitude',	'longitude', cleaningCSV'altitude', 'speed', 'opType']
cols=selected_features # iwil back and fix this line since I would like to generalize to as many as features possible
cols+=['acCat','locid', 'uid']


os.chdir("10")

for num, csvfl in enumerate(sorted(glob.glob('*.csv'))):
    print(csvfl)
    if csvfl[:4]=='test':
        test_type=1
        
    else:
        test_type=0

    filejson=csvfl[csvfl.index('_dca')+1:-4]+"_AO.njson"    

    list_revise=cleaningCSV(dir_fl, filejson,  cols, test_type, list_revise=REVISE_LS)


with open('/home/yousef/Summer19/NN/trajec_fly/Days_with_Anamoly.txt', 'w') as f:
    for item in list_revise:
        f.write("%s\n" % item)





#list_revise len(list_revise)==11

with open('/home/yousef/Summer19/NN/trajec_fly/Days_with_Anamoly.txt', 'r') as f:
    list_revise=f.read().split("\n")


list_revise=list_revise[:-1]

"""
#list_revise len(list_revise)==22

0:  'test_trajectory_dca_pd_20160613_site_10.csv', PASSED
[1030, 1031] time 1465862396 passed keep those

1:  'test_trajectory_dca_pd_20170602_site_10.csv',  PASSED
Int64Index([335, 336], dtype='int64', name='idx') time 1496448000 passed keep those
 
2:  'test_trajectory_dca_pd_20170614_site_10.csv', 1497440606  FAILED
[56, 57, 335]   file 57 is truncated in middle  
   
idx: 56 is passed keep 56, 
idx: 57 Wednesday, June 14, 2017 11:44:00 AM  FAILD UID 50218469 dropped
idx: 335 PASSED Thursday, June 15, 2017 12:00:00 AM UID 50223184

array([27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
       44, 45, 46, 47, 48, 49, 50, 51, 52, 53])
    
6:  'train_trajectory_dca_pd_20161013_site_10.csv', Failed
idx_nans[0]
idx [888, 889]

idx 888 passed

idx 889 passed

7: 'train_trajectory_dca_pd_20161102_site_10.csv', FAILED
[412, 413]
idx 412  passed
413 failed ##

8: 'train_trajectory_dca_pd_20161105_site_10.csv', Failed
[308, 309]
idx 308 passed ut 1478390400
idx 309 Failed

9:  'train_trajectory_dca_pd_20170117_site_10.csv', FAILED
 array([346, 347])
idx 346 passed
idx 347 failed   
 

10: 'train_trajectory_dca_pd_20170120_site_10.csv', FAILED
array([354, 355])
idx 354 passed
idx 355 failed

11: 'train_trajectory_dca_pd_20170317_site_10.csv', FAILED
 array([ 92,  96, 401])
idx 92 passed
idx 96 passed
401 faild UID 47462048

12: 'train_trajectory_dca_pd_20170712_site_10.csv', IN TWO TRAJECTORIES FAILED
array([372, 373])
idx 372 passed 50410883
idx 373 failed uid 50411306 ut=1499903997


13: 'train_trajectory_dca_pd_20171002_site_10.csv', FAILED
array([395, 396])
idx 395 passed
idx 396 failed uid 50964370 ut 1506988793 


#####################################################################################################

double check

3: 'test_trajectory_dca_pd_20160613_site_10.csv', PASSED
idx [1030, 1031]
idx 1030 passed  
idx 1031 passed  no action required


4:  'test_trajectory_dca_pd_20170602_site_10.csv', PASSED
[335, 336]
idx 335 PASSED
idx 336 PASSED



5: 'test_trajectory_dca_pd_20170614_site_10.csv', Failed
[56, 335]
idx 56 df56=df_nois_trajec.loc[56] passed
idx 335 df335=df_nois_trajec.loc[335] passed


14: 'train_trajectory_dca_pd_20161013_site_10.csvif (test_type==0): FAILED

idx array([888, 889])

idx array 888  passed

idx array 889  failed uid 39721424 ut 1476403197

15: 'train_trajectory_dca_pd_20161102_site_10.csv',  FAILED
array([412, 413])
idx 412 ok no modification is needed
idx 413 dropped as it is in list before 41237444 just was in list


16: 'train_trajectory_dca_pd_20161105_site_10.csv', FAILED
idx [308, 309]
idx 308 passed
idx 309 failed uid 41400490 ut 1478390397

17: 'train_trajectory_dca_pd_20170117_site_10.csv', FAILED
 idx array([346, 347])
 idx 346 passed
idx  347 failed uid was on list 44847572 ut=1484697595

18:  'train_trajectory_dca_pd_20170120_site_10.csv', FAILED
  array([354, 355])
idx 354 passed
idx 355 failed uid 45000186 on list ut 1484956799
   
19: 'train_trajectory_dca_pd_20170317_site_10.csv', FAILED
idx array([ 92,  96, 401])
idx 92 passed
idx 96 pass 
401 failed on list UID 47462048


20: 'train_trajectory_dca_pd_20170712_site_10.csv', tWO TRAJECTOREIS FAILED FAILED THE LIST IS IN TOTAL 10 FILES MISSED
 array([372, 373])
 
 idx 372 dropped 50410883
 idx 373 faile d 50411306 was on list
 
21:  'train_trajectory_dca_pd_20171002_site_10.csv' FAILED
    array([395, 396])
    
    395 passed
    396 failed uid 50964370 ut 1506988793
    
    
    
10 TRAJECTORIES WERE MODIFIED IN TOTAL 9 FILES WITH FAIL
1- 'test_trajectory_dca_pd_20170614_site_10.csv'
2- 'train_trajectory_dca_pd_20161013_site_10.csv'
3- 'train_trajectory_dca_pd_20161013_site_10.csv
4- 'train_trajectory_dca_pd_20161105_site_10.csv'
5- train_trajectory_dca_pd_20170117_site_10.csv'
6- 'train_trajectory_dca_pd_20170120_site_10.csv'
7- 'train_trajectory_dca_pd_20170317_site_10.csv'
8- train_trajectory_dca_pd_20170712_site_10.csv
9- 'train_trajectory_dca_pd_20171002_site_10.csv'

"""