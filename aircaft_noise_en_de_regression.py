#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 14:43:25 2019

@author: yousef
"""
import numpy as np
import tensorflow as tf
import pandas as pd
import random as rn
import matplotlib.pyplot as plt
import os
pd.options.display.width = 0
pd.set_option('display.expand_frame_repr', False)
np.random.seed(42)
rn.seed(12345)

session_conf=tf.ConfigProto(intra_op_parallelism_threads=8, inter_op_parallelism_threads=8)
from sklearn.preprocessing import StandardScaler
import keras
from keras.utils import to_categorical
from keras.models import Model
from keras.layers import Input, LSTM, Dense
from keras.optimizers import Adam
from keras import backend as K
sess=tf.Session(graph=tf.get_default_graph(), config=session_conf)

K.set_session(sess)

os.chdir('/home/yousef/Summer19/NN/trajec_fly/') #set  to be your pythonpath

#model specification
thr=150
batch_size=20
epochs=60
latent_dim=200
dropout = 0 

#only needed if python path is different  from storing code
dir_flnm="/home/yousef/Summer19/NN/trajec_fly/"  #folder for import files of belo

# data folder
data_dir="DataCMAPSS/"
filenm_train="train_FD001.txt"  #train file 
filenm_test="test_FD001.txt"    #Xtest file
response="RUL_FD001.txt"        #y_test file

#generating header for csv files in the 
colnms = ['unitid', 'time', 'set_1','set_2','set_3']

colnms.extend(['sensor_' + str(i) for i in range(1,22)])
 
#not useful information mostely fixed values
 
rm_cols=['set_3', 'sensor_1', 'sensor_5', 'sensor_6', 'sensor_10', 'sensor_16', 'sensor_18', 'sensor_19']

unit_time=['unitid','time']


selected_features=list(set(colnms)-set(['unitid',  'time', 'set_3', 'sensor_1',\
                       'sensor_5', 'sensor_6', 'sensor_10', 'sensor_16', 'sensor_18', 'sensor_19']))

#prepare train
runfile('prepTrainData.py')

scaler, X_train, y_train, max_len_train, part_arr_class_train= prepDataTrain(
        dir_flnm=data_dir, filenm=filenm_train, thr=150, colnms=colnms,\
        selected_features=selected_features, unit_time=unit_time, rm_cols=rm_cols)

# train the rnn lstm model
runfile("rnn_lstm_reg_model_training.py")

history =model.fit_generator(
        train_generator(X_train,y_train, max_len_train=max_len_train)\
        , verbose=1, epochs=epochs, steps_per_epoch=20, validation_split=.2)  


##save model weights h5 
model.save('saved_wt_rnn_seq2seq_reg.h5')

#model.save('model_rnn_seq2seq_reg.h5')

plt.plot(history.history['loss'])
plt.xlabel('Epoch')
plt.ylabel('RMSE')
plt.title('Root-Mean-Square Error')
plt.legend(['Train'])


#prepare test data

runfile('prepTestData.py')


X_test, y_test, max_len_test, part_arr_class_test=prepDataTest(
        dir_flnm=data_dir, filenm_test=filenm_test, response=response, scaler=scaler, thr=150,\
        colnms=colnms, selected_features=selected_features, unit_time=unit_time, rm_cols=rm_cols)


#evaluate model
runfile('rnn_lstm_reg_model_testing_evaluation.py')


score = model.evaluate_generator(
        train_generator(
                X_test,y_test, max_len_train=max_len_test
                ), int(100/20), verbose=1) #ofsamples/batchsize

loss, acc= score.copy()
#[8.870702934265136, 0.8046800971031189]

print('\n ACC Test accuracy: {0}'.format(acc))
# ACC Test accuracy: 0.8046800971031189
print('\n Test RMSE: {0}'.format(loss))

history_test =model.fit_generator(
        train_generator(X_train,y_train, max_len_train=max_len_train)\
        , verbose=1, epochs=epochs, steps_per_epoch=20)

##save h5 model
test_model.save('evaluate_rnn_seq2seq_reg.h5')


interv_21, y_test_21, pred_mat_test_21 =predfun(
        21, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)

interv_95, y_test_95, pred_mat_test_95 =predfun(
        95, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)


interv_15, y_test_15, pred_mat_test_15 =predfun(
        15, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)


interv_80, y_test_80, pred_mat_test_80 =predfun(
        80, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)


import matplotlib.pyplot as plt

fig, ax = plt.subplots(2, 2)

ax[0, 0].plot(interv_95,  y_test_95,  '--', c='blue', label='actual') 
ax[0, 0].plot(interv_95, pred_mat_test_95,  'o-', c='orange',label='prediction')
ax[1, 0].plot(interv_21, y_test_21,  '--', c='blue', label='actual') 
ax[1, 0].plot(interv_21, pred_mat_test_21.flatten(), 'o-', c='orange', label='prdiction') 
ax[0, 1].plot(interv_15, y_test_15,  '--', c='blue', label='actual')
ax[0, 1].plot(interv_15, pred_mat_test_15, 'o-', c='orange', label='prediction')
ax[1, 1].plot(interv_80, y_test_80,  '--', c='blue', label='actual')
ax[1, 1].plot(interv_80, pred_mat_test_80, 'o-', c='orange', label='prediction')
ax[0, 0].set(title="Test Observation 95", xlabel="Time Step")
ax[0, 1].set(title="Test Observation 15", xlabel="Time Step")
ax[1, 0].set(title="Test Observation 21", xlabel="Time Step")
ax[1, 1].set(title="Test Observation 80", xlabel="Time Step")
plt.legend(['Target Series','Predictions'])

plt.show()


