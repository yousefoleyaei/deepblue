#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 14:43:25 2019

@author: yousef
"""
import numpy as np
import tensorflow as tf
import pandas as pd
import random as rn
import matplotlib.pyplot as plt
import os

pd.options.display.width = 0
pd.set_option('display.expand_frame_repr', False)
np.random.seed(42)
rn.seed(12345)

session_conf=tf.ConfigProto(intra_op_parallelism_threads=8, inter_op_parallelism_threads=8)
from sklearn.preprocessing import StandardScaler
import keras
from keras.utils import to_categorical
from keras.models import Model
from keras.layers import Input, LSTM, Dense
from keras.optimizers import Adam
from keras import backend as K
import gc


    

def fun_graphics_gre_val(tru_y, mod_arrpred):

    true_y=np.array(tru_y, copy=True)

    idx_val=np.where((tru_y==tru_y) & (tru_y!=0))

    predicted_y=fun_nans_arr(tru_y)#*predicted_y.shape(1)*predicted_y.shape(0)).reshape(predicted_y.shape(0), predicted_y.shape(1),predicted_y.shape(2))
    predicted_y[idx_val]=mod_arrpred[idx_val]
    predicted_y=predicted_y.reshape(100,-1)
    
#    temp_pred=np.array(true_y, copy=True)
    true_y=fun_nans_arr(true_y)
    true_y[idx_val]=tru_y[idx_val]
    true_y=true_y.reshape(100,-1)
    
    from matplotlib import pyplot as plt
    
    #fresh instances of matplotlib
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #i=0
    for i in np.arange(100):
    #    i=1
        idx_pred=np.where(true_y[i]>50)
        if len(idx_pred[0])>0:#plot non-null values         
            ax.scatter(idx_pred[0],\
                            true_y[i][idx_pred], marker='^', label=['Actural values'], )
            ax.scatter(idx_pred[0],\
                        predicted_y[i][idx_pred], marker= 'o', label=['Predictions'], )
                
    colormap = plt.cm.gist_ncar #nipy_spectral, Set1,Paired  
    colorst = [colormap(i) for i in np.linspace(0, 0.9,len(ax.collections))]       
    for t,j1 in enumerate(ax.collections):
        j1.set_color(colorst[t])
        
    ax.set(ylabel="Noise (db)", xlabel="Time step", title="Actual versus prediction on training")
    
    ax.legend(['Actual values', 'Predictions'], fontsize='small') 
    plt.savefig("gre_eq_values.png")
    plt.show()



def fun_nans_arr(predicted_y):
    predicted_y=np.empty(predicted_y.shape, dtype='float')
    a=predicted_y
    a.fill(np.nan)
    return a


sess=tf.Session(graph=tf.get_default_graph(), config=session_conf)

K.set_session(sess)

os.chdir("/home/yousef/Summer19/NN/trajec_fly/")
dir_flnm="" #"10\"
selected_features=['altitude', 'latitude', 'longitude', 'speed', 'opType']#, 'opType_D']

#for trining in whole data it is ignored
#this is cv not  needed in whole model verification"
epochs=100 #chng from 60
hist=[] # next I will add more data for training

# 5-fold cross validation
#validation_split=.2

for J in np.arange(5):
    
#    runfile('training_validation.py') #every fresh instances change it to ever time a fresh form model saved in disk instead of building it
    runfile('/home/yousef/Summer19/Archive/val_seq2seq_reg_model.py')
    #validation_split=.2
    history =model.fit_generator(
            train_generator1(X_train,y_train, max_len_train=max_len_train,j=J)\
            , validation_data = validation_generator1(X_train,y_train, max_len_train=max_len_train,j=J), 
        validation_steps = 4,verbose=1, epochs=epochs, steps_per_epoch=16 )
    initial_weights = model.get_weights()    
    k_eval = lambda placeholder: placeholder.eval(session=K.get_session())
	#removing weights
    new_weights = [k_eval(glorot_uniform()(w.shape)) for w in initial_weights]
    model.set_weights(new_weights)
    K.clear_session()
    model.reset_states()
    gc.collect()
    del model
	#delete model and prepare for next fold
    hist.append(history)
#for J in np.arange(5):

    #plotting cv result
    plt.plot(hist[J].history[ 'loss'], label='loss_'+'fold_'+str(J))
    plt.plot(hist[J].history[ 'val_loss'], label='val_loss_'+'fold_'+str(J))
    plt.xlabel('Epoch')
    plt.ylabel('RMSE')
    plt.title('Five-Fold Cross-Validation for Aircraft Noise Model (RMSE)')
    plt.legend()

#prepare train
#runfile("prepFly_X_train_Data.py")
#read from disc
#scaler, X_train, y_train, max_len_train, part_arr_class_train= prepDataTrain(
#        dir_flnm=dir_flnm, filenm=filenm, selected_features=selected_features)

#
#4 continus nozero and one categorical
    
X_train_grand=np.load("wo_cat_trans_X_train_clean_train_dca_pd_20160611_site_10.npy")
y_train_grand=np.load("wo_cat_trans_y_train_clean_train_dca_pd_20160611_site_10.npy")
X_train=X_train_grand[-100:,:,:]
y_train=y_train_grand[-100:,:,:]
X_train[X_train!=X_train]=0 #commentet out for training on nan purpose
y_train[y_train!=y_train]=0

#for 
#hist=[]

from keras.initializers import glorot_uniform  # Or your initializer of choice
import keras.backend as K
dropout=0
#runfile('seq2seq_reg_model.py')
#filter out on arrivals #wo_cat_trans_dict_len_clean_train_dca_pd_20160611_site_10.csv

max_len_train=pd.read_csv('wo_cat_trans_dict_len_clean_train_dca_pd_20160611_site_10.csv')

max_len_train=max_len_train['jetlen'].iloc[-100:].values
max_len_train=max_len_train.reshape(-1,20)



runfile('/home/yousef/Summer19/Archive/val_seq2seq_reg_model.py')



history=model.fit_generator(train_generator(X_train,y_train, max_len_train=max_len_train)\
                               ,verbose=1, epochs=epochs, steps_per_epoch=100//20)

score_train=model.evaluate_generator( 
        train_generator(
                X_train,y_train, max_len_train=max_len_train
                ), int(100/20), verbose=1)

plt.plot(history.history[ 'loss'], label='extreme long sequences')
plt.xlabel('Epoch')
plt.ylabel('MSE')
plt.title('Seq2Seq Training Loss for Aircraft Noise Model')
plt.legend()

in_idx=np.arange(100)

de_train=fun_dn_in(y_train, max_len_train, in_idx)

    
def fun_inverse(x):
    
    x[x<0 & (x!=0)]=50
    x[(x>50) | ((x>0) & (x<50))]=50+np.log(x[(x>50) | ((x>0) & (x<50))])
    return x


arrpred=model.predict([X_train, de_train])

mod_arrpred=np.copy(arrpred)
mod_arrpred=fun_inverse(mod_arrpred)

y_true=np.copy(y_train)
y_true=fun_inverse(y_true)


mask_y_t=np.where((y_true!=0) & (y_true==y_true)) #(& (y_train!=np.nan)& (y_train==y_train)) these are necessary for customize batch
masked_y_t=y_true[mask_y_t]
masked_arrpred=mod_arrpred[mask_y_t]

from matplotlib import pyplot as plt

sqer_mask=np.mean((masked_arrpred-masked_y_t)**2) #5.803586557888101 extreme null as zero has greater error then 3.84 with null but predicts null


df_err=pd.DataFrame(np.array([masked_y_t, masked_arrpred]).reshape(-1,2), index=np.arange(len(masked_y_t)))

df_err.reset_index(inplace=True,drop=True)
df_err['Err']=(df_err[1]-df_err[0])**2
sqer_mask=np.sqrt(df_err['Err'].mean())
#3.8786572482078325 #neumann
df_err.columns=['Actual values', 'RNN seq2seq predictions', 'Error for training during learning']
df_err.to_csv("SGD_Train_date11.csv", index=0, encoding='utf8')
ax=df_err[['Actual values', 'RNN seq2seq predictions']].plot(title="Training predictions")
ax.set_ylabel=("Noise (db)")
ax.set_xlabel=(["Joined long sequences (training)"])

###############################################################################
    
fun_graphics_gre_val(true_y, predicted_y)


fun_plot_stich(df_err, max_len_train, "train")

#********************************#********************************#********************************
X_train1=np.load("X_test_clean_test_trajectory_dca_pd_20160612_site_10_AO.npy")
y_train1=np.load("y_test_clean_test_trajectory_dca_pd_20160612_site_10_AO.npy")
X_train2=X_train1[:100,:,:]
y_train2=y_train1[:100,:,:]
X_train_in=X_train2
y_train_out=y_train2
X_train_in[X_train_in!=X_train_in]=0
y_train_out[y_train_out!=y_train_out]=0


max_len_valid=pd.read_csv('final_dict_len_clean_test_trajectory_dca_pd_20160612_site_10_AO.csv',\
                          nrows=100)
max_len_valid=max_len_valid['jetlen'].values.reshape(-1,20)
   

in_idx_out=np.arange(100) #100 columns were chosen. # extend it to more
de_train_in=fun_dn_in(y_train_out, max_len_valid, in_idx_out)


#hist_train=model.fit_generator(train_generator(X_train,y_train, max_len_train=max_len_valid)\
#                               ,verbose=1, epochs=epochs, steps_per_epoch=20,)

score_train=model.evaluate_generator(
        train_generator(
                X_train_in,de_train_in, max_len_train=max_len_valid
                ), int(100/20), verbose=1)


arrpred_in=model.predict([X_train_in, de_train_in])
#first senario test rmse on all the predictions

mod_arrpred=np.copy(arrpred_in)


mod_arrpred=fun_inverse(mod_arrpred)

act_y_train_out=fun_inverse(y_train_out)

sqerr=np.mean((mod_arrpred-y_train_out)**2) #6.947101654722803 neumann

mask_y_test=np.where(y_train_out!=0)
masked_y_test=y_train_out[mask_y_test]
masked_arrpred_test=mod_arrpred[mask_y_test]

sqer_mask=np.mean((masked_arrpred_test-masked_y_test)**2) 

df_err_test=pd.DataFrame(np.array([masked_y_test, masked_arrpred_test]).T, index=np.arange(len(masked_arrpred_test)))



#*****************************************************************************
def fun_plot_stich(df_err, max_len_train, nm):
    
    df_err_test=df_err.dropna()
    df_err_test['MSE']=(df_err_test[1]-df_err_test[0])**2
    sqer_mask_df=df_err_test['MSE'].mean()
    print(sqer_mask_df)
    #4.779153939736943
    df_err_test.reset_index(inplace=True,drop=True)
    df_err_test.columns=['Actual values', 'RNN seq2seq predictions', 'MSE']
    ls_clip=max_len_train.flatten().cumsum()-1 #1

    fig, ax = plt.subplots()
    ax = plt.gca()
    #ax.grid(True, xdata=ls_clip)
    ax.grid(True, axis='x', linestyle='--', )#, xdata=ls_clip)
    df_err_test[['Actual values', 'RNN seq2seq predictions']].plot(title="Prediction on stiched long sequences with length between 7 and 58", ax=ax)
    #ax.plot(masked_reg, label="generalized linear regression")
    ax.set_xticks(np.array([0]+ls_clip.tolist()), minor=False)
    ax.grid(True, axis='x', linestyle='--', )#, xdata=ls_clip)
    ax.set_ylabel("Noise level (db)")
    ax.set_xlabel("Stiched different short time series length")
    #plt.set_xticks(rotation=-45, ha='center')
    ax.legend()
    ax.set_xticklabels=([""])
    
    df_err.to_csv(nm+"_stiched_sgd_train_seq_len_longest_sequence_en_de.csv", index=0, encoding='utf8')
    return df_err_test



fun_plot_stich(df_err_test, max_len_valid, "Test")
#df_err_test.to_csv("SGA_MSE_test_seq_len_5_en_de.csv", index=0, encoding='utf8')

#***********************************************************#
true_y=y_train_out

idx_val=np.where((true_y==true_y) & (true_y!=0))

predicted_y=np.empty(y_train_out.shape, dtype='float')
def nans(predicted_y):
    a=predicted_y
    a.fill(np.nan)
    return a

predicted_y=nans(predicted_y)#*predicted_y.shape(1)*predicted_y.shape(0)).reshape(predicted_y.shape(0), predicted_y.shape(1),predicted_y.shape(2))
predicted_y[idx_val]=mod_arrpred[idx_val]
predicted_y=predicted_y.reshape(100,-1)

temp_pred=true_y
true_y=nans(np.empty(true_y.shape, dtype='float'))
true_y[idx_val]=temp_pred[idx_val]
true_y=true_y.reshape(100,-1)


from matplotlib import pyplot as plt


for i in np.arange(100):
    plt.plot(true_y[i], color='blue', label='Actural values')
    plt.plot(predicted_y[i], c='red', label='RNN en_decoder Predictions')
    
fig, ax = plt.subplots()
ax = plt.gca()    
for i in np.arange(100):
    
    if len(true_y[i][true_y[i]>=50]):
        idx_pred=np.where(true_y[i]>=50)
        print(i)
        ax.plot(true_y[i][idx_pred], color='blue', label='Actural values')
        ax.plot(predicted_y[i][idx_pred], c='red', label='RNN en_decoder Predictions')

ax.set_xlabel=("time step")
plt.ylabel("RMSE")
plt.title("Time series predictions on test short sequences")
plt.legend(['Actural values','RNN en_decoder Predictions' ])
plt.savefig('SGA_timeseries_test_on_short sequences.png')
plt.show()



#write code to eliminate x prediction with value non zero to zeros
#################################################

X_train1=np.load("X_test_clean_test_trajectory_dca_pd_20160612_site_10_AO.npy")
y_train1=np.load("y_test_clean_test_trajectory_dca_pd_20160612_site_10_AO.npy")
X_train2=X_train1[-100:,:,:]
y_train2=y_train1[-100:,:,:]
X_train_in=X_train2
y_train_out=y_train2
X_train_in[X_train_in!=X_train_in]=0
y_train_out[y_train_out!=y_train_out]=0
#de_train_in=np.zeros(y_train_out.shape)
#de_train_in[:,1:,:]=y_train_out[:,:-1,:]
#de_train_in[:,0,:]=y_train_out[:,0,:]

max_len_valid=pd.read_csv('final_dict_len_clean_test_trajectory_dca_pd_20160612_site_10_AO.csv',\
                          )
max_len_valid=max_len_valid['jetlen'].tail(100).values.reshape(-1,20)


score_train=model.evaluate_generator(
        train_generator(
                X_train_in,y_train_out, max_len_train=max_len_valid
                ), int(100/20), verbose=1)

i=0
de_train_ting=fun_en_in_solid(y_train_out[20*i:20*(1+i)], max_len_valid, 0)

for i in np.arange(1,5):
    de_train_ting=np.concatenate((de_train_ting, np.array(fun_en_in_solid(y_train_out[20*i:20*(1+i)], max_len_valid, i))), axis=0)
    
    
de_train_in=de_train_ting



arrpred_in=model.predict([X_train_in, de_train_in])

sqerr=np.sqrt(np.mean((arrpred_in-y_train_out)**2)) #6.947101654722803 neumann

mask_y_test=np.where(y_train_out!=0)
masked_y_test=y_train_out[mask_y_test]
masked_arrpred_test=arrpred_in[mask_y_test]

sqer_mask=np.sqrt(np.mean((masked_arrpred_test-masked_y_test)**2)) #4.330795143585938 with correct steps per epch

df_err_test=pd.DataFrame(np.array([masked_y_test, masked_arrpred_test]).T, index=np.arange(len(masked_arrpred_test)))
df_err_test=df_err_test.dropna()
df_err_test['Err']=(df_err_test[1]-df_err_test[0])**2
sqer_mask_df=np.sqrt(df_err_test['Err'].mean())
#4.779153939736943
df_err_test.reset_index(inplace=True,drop=True)
df_err_test.columns=['act', 'rnn_pred', 'err']
#***************************************************








#**************************
ls_clip=max_len_valid.flatten().cumsum()-1

fig, ax = plt.subplots()
ax = plt.gca()
#ax.grid(True, xdata=ls_clip)
ax.grid(True, axis='x', linestyle='--', )#, xdata=ls_clip)
df_err_test[['act','rnn_pred']].plot(title="prediction on stiched short sequences with length between 5 and 7", ax=ax)
ax.plot(masked_reg, label="generalized linear regression")
ax.set_xticks(np.array([0]+ls_clip.tolist()), minor=False)
ax.set_ylabel("noise level (db)")
ax.set_xlabel("stiched different long time test series length")
plt.xticks(rotation=-45, ha='center')
ax.legend()
plt.set_xticklabels=([""])

df_err_test.to_csv("RMSE_extremely_large_seq_len_5_en_de.csv", index=0, encoding='utf8')

#***********************************************************#
true_y=y_train_out

idx_val=np.where((true_y==true_y) & (true_y!=0))

predicted_y=np.empty(y_train_out.shape, dtype='float')
def nans(predicted_y):
    a=predicted_y
    a.fill(np.nan)
    return a

predicted_y=nans(predicted_y)#*predicted_y.shape(1)*predicted_y.shape(0)).reshape(predicted_y.shape(0), predicted_y.shape(1),predicted_y.shape(2))
predicted_y[idx_val]=arrpred_in[idx_val]
predicted_y=predicted_y.reshape(100,-1)

temp_pred=true_y
true_y=nans(np.empty(true_y.shape, dtype='float'))
true_y[idx_val]=temp_pred[idx_val]
true_y=true_y.reshape(100,-1)


from matplotlib import pyplot as plt


for i in np.arange(100):
    plt.plot(true_y[i].flatten(), color='blue', label='Actural values')
    plt.plot(predicted_y[i].flatten(), c='red', label='RNN en_decoder Predictions')
    
plt.legend(['Actural values', 'RNN en_decoder Predictions'])    
plt.legend()
plt.xlabel("time step")
plt.ylabel("Noise (db)")
plt.title("Time series predictions on test longest sequences")
plt.legend(['Actural values','RNN en_decoder Predictions' ])
plt.savefig('sga timeseries test on long sequences.png')
plt.show()



plt.plot(true_y[i].flatten(), color='blue', label='Actural values')
















###############################################################
#train section
rom sklearn import linear_model

glm_reg = linear_model.LinearRegression()

X_train_reg=X_train.reshape(-1,4)
y_train_reg=y_train.reshape(-1,1)
glm_reg.fit(X_train_reg,y_train_reg)


reg_arrpred=glm_reg.predict(X_train_reg)
#vs 3.891302501689761
# point something 
sqerr_reg=np.sqrt(np.mean((reg_arrpred-y_train_reg)**2))
#  21.392118279608038 worse than  [10.088344192504882, 0.13951063230633737] #neumann
#model rnn on traning
mask_y_reg=np.where(y_train_reg!=0)
masked_y_train_reg=y_train_reg[mask_y_reg]

masked_arrpred_reg=reg_arrpred[mask_y_reg]

#pd.DataFrame([])
sqer_mask_train_reg=np.sqrt(np.mean((masked_arrpred_reg-masked_y_train_reg)**2))

#36.907126015781266 #neumann result vs
# vs sqer_mask  [8.690413570404052, 0.013190476410090923] onumann n training now its test


###############################################################
#test section
X_train_reg_pred=X_train_in.reshape(-1,4)
y_train_reg_pred=y_train_out.reshape(-1,1)

reg_arrpred_train=glm_reg.predict(X_train_reg_pred)

sqerr_reg=np.sqrt(np.mean((reg_arrpred_train-y_train_reg_pred)**2))
#16.814290422522564 versus 6.212066736889443  #model rnn on test

mask_y_train_reg=np.where(y_train_reg_pred!=0)
masked_y_reg=y_train_reg_pred[mask_y_train_reg]

pred_reg_masked=reg_arrpred_train[mask_y_train_reg]

#pd.DataFrame([])
sqer_mask_reg=np.sqrt(np.mean((masked_reg-masked_y_reg)**2))

#38.872512469933305 vs  3.891302501689761 
#1) adding autoregressive for glm and 
#2) fit on non nan similar to parital padding
#shoud tast without nan and fitting on non zero and null 
#plt.plot(masked_y_reg)
#plt.plot(pred_reg_masked)
###############################################################
#train section with non zero
X_train_reg=X_train_grand[-100:].reshape(-1,4)
y_train_reg=y_train_grand[-100:].reshape(-1,1)
X_train_reg=X_train_reg[(X_train_reg!=0) & (X_train_reg==X_train_reg)].reshape(-1,4)
y_train_reg=y_train_reg[(y_train_reg!=0) & (y_train_reg==y_train_reg)].reshape(-1,1)
glm_reg.fit(X_train_reg,y_train_reg)

y_pred_individual=glm_reg.predict(X_train_reg)

rmse_train_mask_reg=np.sqrt(np.mean((y_pred_individual-y_train_reg)**2))

def fun_rtn_post(indep_arr, dep_arr, time_span):
    in_arr=indep_arr[time_span]
    dep_arr=dep_arr[time_span]
    in_arr[in_arr!=in_arr]=0
    dep_arr[dep_arr!=dep_arr]=0
    dep_arr=dep_arr.reshape(-1,58)
    idx_arr_non0=np.where(dep_arr!=0)
    in_arr_non0=in_arr[idx_arr_non0].reshape(-1,4)
    de_arr_non0=dep_arr[idx_arr_non0].reshape(-1,1)
    """
    idx_in_arr_non0=in_arr[(in_arr!=0) & (in_arr==in_arr)].reshape(-1,4)
    y_train_reg=y_train_reg[(y_train_reg!=0) & (y_train_reg==y_train_reg)].reshape(-1,1)
    """
    return in_arr_non0, de_arr_non0.flatten()

in_train, de_train= fun_rtn_post(X_train_grand, y_train_grand, np.arange(-100,0))


from sklearn import linear_model

glm_reg = linear_model.LinearRegression()

glm_reg.fit(in_train,de_train)


y_pred_individual=glm_reg.predict(in_train)

rmse_train_nonzero_reg=np.sqrt(np.mean((y_pred_individual-de_train)**2))

#*******************************************************
#test nonzero 

in_test, de_test= fun_rtn_post(X_train_grand, y_train_grand, np.arange(100))

y_pred_test_individual=glm_reg.predict(in_test)

rmse_test_nonzero_reg=np.sqrt(np.mean((y_pred_test_individual-de_test)**2))


plt.plot(de_test)
plt.plot(masked_arrpred_test)
plt.plot(y_pred_test_individual) #


#adding auto regression part
################################################################
def fun_prep_auto(indep_arr, dep_arr, time_span):
    in_arr=indep_arr[time_span]
    dep_arr=dep_arr[time_span]
    in_arr[in_arr!=in_arr]=0
    dep_arr[dep_arr!=dep_arr]=0
    auto_dep=np.concatenate((dep_arr[0].reshape(1,58,-1),dep_arr[:-1,:]), axis=0)
    in_arr=np.concatenate((in_arr, auto_dep.reshape((auto_dep.shape[0], auto_dep.shape[1], -1))), axis=2 )
    dep_arr=dep_arr.reshape(-1,58)
    idx_arr_non0=np.where(dep_arr!=0)
    in_arr_non0=in_arr[idx_arr_non0].reshape(-1,5)
    de_arr_non0=dep_arr[idx_arr_non0].reshape(-1,1)
    """
    idx_in_arr_non0=in_arr[(in_arr!=0) & (in_arr==in_arr)].reshape(-1,4)
    y_train_reg=y_train_reg[(y_train_reg!=0) & (y_train_reg==y_train_reg)].reshape(-1,1)
    """
    return in_arr_non0, de_arr_non0.flatten()


from sklearn import linear_model

glm_reg = linear_model.LinearRegression()





in_train_auto, de_train_auto= fun_prep_auto(X_train_grand, y_train_grand, np.arange(-100,0))

""""
indep_arr=X_train_grand

dep_arr=y_train_grand
time_span=np.arange(-100,0)
"""





glm_reg.fit(in_train_auto,de_train_auto)


y_pred_auto_individual=glm_reg.predict(in_train_auto)

rmse_train_auto_reg=np.sqrt(np.mean((y_pred_auto_individual-de_train_auto)**2))



auto_in_test, de_test= fun_prep_auto(X_train_grand, y_train_grand, np.arange(100))

y_pred_test_auto_individual=glm_reg.predict(auto_in_test)

rmse_test_auto_nonzero_reg=np.sqrt(np.mean((y_pred_test_auto_individual-de_test)**2))


plt.plot(de_test)
plt.plot(masked_arrpred_test)
plt.plot(y_pred_test_auto_individual) #

############################################

df_err_test_auto=pd.DataFrame(np.array([de_test, masked_arrpred_test, y_pred_test_auto_individual]).T, columns=['Actual values', 'RNN en-decoder predictions', 'Autoregression predictions'],\
                              index=np.arange(len(masked_arrpred_test)))

#"""better plotting deliveries
fig, ax = plt.subplots(figsize=(6, 4))
ax = plt.gca()
#ax.xaxis.set_xdata(ls_clip.tolist())
df_err_test_auto.plot(title="Predictions with different time series with length of 5, 6, and 7", ax=ax)
#ax.plot(masked_reg, label="generalized linear regression")
ls_clip=max_len_valid.flatten().cumsum()-1
#ax.grid(True, xdata=ls_clip)
ax.grid(True, axis='x', linestyle='--', )#, xdata=ls_clip)
ax.set_xticks(np.array([0]+ls_clip.tolist()), minor=False)
plt.setp( ax.get_xticklabels(), visible=False)
ax.set_ylabel("Noise level (db)")
ax.set_xlabel("Different length short time series stiched togheter")
plt.xticks(rotation=-45, ha='center')
ax.legend()

df_err_test_auto.to_csv("Auto_seq_len_5_en_de.csv", index=0, encoding='utf8')








#parameters
""""
indep_arr=X_train_grand

dep_arr=y_train_grand
idx=100
"""
###############################################################
max_len_grand=pd.read_csv('Max_len_clean_quard_train_trajectory_dca_pd_20160611_site_10_AO.csv')
max_len_test=max_len_grand['jetlen'].iloc[-200:-100].values
max_len_test=max_len_test.reshape(-1,20)

X_test_in=X_train_grand[-200:-100,:,:]
y_test_out=y_train_grand[-200:-100,:,:]
X_test_in[X_test_in!=X_test_in]=0
y_test_out[y_test_out!=y_test_out]=0
de_test_in=np.zeros(y_test_out.shape)
de_test_in[:,1:,:]=y_test_out[:,:-1,:]
de_test_in[:,0,:]=y_test_out[:,0,:]


score_test=model.evaluate_generator(
        train_generator(
                X_test_in,y_test_out, max_len_train=max_len_test
                ), int(100/20), verbose=1)
#[8.940971088409423, 0.01636904813349247] #neumann
arrpred_test=model.predict([X_test_in, de_test_in])
#max_len_train[:5].sum()
#core_test
#Out[57]: [12.436617946624756, 0.012341131176799535]


mask_y_test=np.where(y_test_out!=0)
masked_y_test=y_test_out[mask_y_test]
masked_arrpred_test=arrpred_test[mask_y_test]


#pd.DataFrame([])
sqer_mask_test=np.sqrt(np.mean((masked_arrpred_test-masked_y_test)**2))
#5.139881752556873 neumann
#7.5149186012762845 many to many err
# testing this many to many with multiple regression

from sklearn import linear_model

glm_reg = linear_model.LinearRegression()

X_train_reg=X_train_in.reshape(-1,4)
y_train_reg=y_train_out.reshape(-1,1)
reg_arrpred_train=glm_reg.predict(X_train_reg)

sqerr_reg=np.sqrt(np.mean((reg_arrpred_train-y_train_reg)**2))
#13.555733277139485 worse than model rnn on traning

mask_y_train_reg=np.where(y_train_reg!=0)
masked_y_train_reg=y_train_reg[mask_y_train_reg]

masked_arrpred_reg=reg_arrpred_train[mask_y_train_reg]





#pd.DataFrame([])
sqer_mask_train_reg=np.sqrt(np.mean((masked_arrpred_reg-masked_y_train_reg)**2))
#41.89288995121191
#masked_arrpred_reg[:5] #the length of nonzero elements in list is 7
# this is less powerful test because sequences might have different length i.e. trained on length 5--7\
# tested on length 6 and 5
#what if model training for long sequences and using it for short sequences in test set




#evaluate on test
X_test_reg=X_test_in.reshape(-1,4)
y_test_reg=y_test_out.reshape(-1,1)
reg_arrpred_test=glm_reg.predict(X_test_reg)

sqerr_reg_test=np.sqrt(np.mean((reg_arrpred_test-y_test_reg)**2))

# 17.598859028029736 vs 12.436617946624756 rnn


mask_y_test_reg=np.where(y_test_reg!=0)
masked_y_test_reg=y_test_reg[mask_y_test_reg]

masked_arrpred_reg_test=reg_arrpred_test[mask_y_test_reg]

sqer_mask_test_reg=np.sqrt(np.mean((masked_y_test_reg-masked_arrpred_reg_test)**2))
#43.94551117728339 for reg vs 41.89288995121191 for rnn
# after commit bebd960..dc067f5 

#I can perfom training and test on equal length instead I go further and train model with differnt lenght and use it 
#to predict seq of length 6
#Insted I can do better job with different sequence length and measure the performance at one of them.









"""
de_=y_train[22,:5,:].reshape(1,5,-1)

y_in=y_train[23,:5,:].reshape(1,5,-1)

x_in=X_train[23,:5,:].reshape(1,5,-1)


model.predict([x_in, de_])

array([[[43.493423],
        [43.773838],
        [43.81704 ],
        [43.823082],
        [43.82393 ]]], dtype=float32)
    
    
    vs 
array([[[60.3],
        [65.8],
        [68.7],
        [70.3],
        [71.7]]])
    
"""
    
i=0
plt.plot(hist0.history[ 'loss'], label='loss_'+'fold W_'+str(i))
plt.plot(hist0.history[ 'val_loss'], label='val_loss_'+'fold_'+str(i))
plt.xlabel('Epoch')
plt.ylabel('RMSE')
plt.title('Five-Fold Cross-Validation for Aircraft Noise Model (RMSE)')
    
#add batch with diferrent length

history=model.fit([X_train[:,::-1,:],de_in[:,::-1,:]], y_train[:,::-1,:],
                     batch_size=10,
                     epochs=epochs,
                     validation_split=0.2)



runfile("fly_noise_training_validation.py")
history =model.fit_generator(
    train_generator1(X_train,y_train, max_len_train=max_len_train,j=3)\
    , validation_data = validation_generator1(X_train,y_train, max_len_train=max_len_train,j=J), 
validation_steps = 12,verbose=1, epochs=epochs, steps_per_epoch=18 )

##save model weights h5 
model.save('partial_saved_wt_rnn_seq2seq_reg.h5')
model.save('model_rnn_seq2seq_reg.h5')

#prepare test data
runfile('prepTestData.py')

X_test, y_test, max_len_test, part_arr_class_test=prepDataTest(
        dir_flnm=data_dir, filenm_test=filenm_test, response=response, scaler=scaler, thr=150,\
        colnms=colnms, selected_features=selected_features, unit_time=unit_time, rm_cols=rm_cols)

#evaluate model
runfile('rnn_lstm_reg_model_testing_evaluation.py')

score = model.evaluate_generator(
        train_generator(
                X_test,y_test, max_len_train=max_len_test
                ), int(100/20), verbose=1) #ofsamples/batchsize
loss, acc= score.copy()
#[8.870702934265136, 0.8046800971031189]

print('\n ACC Test accuracy: {0}'.format(acc))
# ACC Test accuracy: 0.8046800971031189
print('\n Test RMSE: {0}'.format(loss))

##save h5 model
test_model.save('evaluate_rnn_seq2seq_reg.h5')


interv_21, y_test_21, pred_mat_test_21 =predfun(
        21, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)

interv_95, y_test_95, pred_mat_test_95 =predfun(
        95, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)


interv_15, y_test_15, pred_mat_test_15 =predfun(
        15, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)


interv_80, y_test_80, pred_mat_test_80 =predfun(
        80, X_test=X_test, y_test= y_test, max_len_test=max_len_test, part_arr_class_test=part_arr_class_test)

#plotting the forecasts
#this section is based on previous true-value ys instead of y-pred to foreccast next time step 
import matplotlib.pyplot as plt

fig, ax = plt.subplots(2, 2)

ax[0, 0].plot(interv_95,  y_test_95,  '--', c='blue', label='actual') 
ax[0, 0].plot(interv_95, pred_mat_test_95,  'o-', c='orange',label='prediction')
ax[1, 0].plot(interv_21, y_test_21,  '--', c='blue', label='actual') 
ax[1, 0].plot(interv_21, pred_mat_test_21.flatten(), 'o-', c='orange', label='prdiction') 
ax[0, 1].plot(interv_15, y_test_15,  '--', c='blue', label='actual')
ax[0, 1].plot(interv_15, pred_mat_test_15, 'o-', c='orange', label='prediction')
ax[1, 1].plot(interv_80, y_test_80,  '--', c='blue', label='actual')
ax[1, 1].plot(interv_80, pred_mat_test_80, 'o-', c='orange', label='prediction')
ax[0, 0].set(title="Test Observation 95", xlabel="Time Step")
ax[0, 1].set(title="Test Observation 15", xlabel="Time Step")
ax[1, 0].set(title="Test Observation 21", xlabel="Time Step")
ax[1, 1].set(title="Test Observation 80", xlabel="Time Step")
plt.legend(['Target Series','Predictions'])

plt.show()


