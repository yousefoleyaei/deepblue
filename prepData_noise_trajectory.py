# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 21:38:08 2019

@author: Youse
1t prep
"""
import numpy as np
import pandas as pd
from pandas import DataFrame as DF
from pandas import Series as Sr
import json

#dir stored test and train
dir_fl='10/'
 
test_type=0
# Ishould change idx to uid to consider multiple lines of same aircraft
fl_nm="dca_pd_20160625_site_10_AO.njson" #first njoison odered by nameto train next 12 is test

def cleaningJs(dir_fl, fl_nm, test_type):
    
    cols=['idx', 't', 'ut','latitude', 'longitude', 'altitude', 'speed']
    cols_traj=cols+['uid', 'opType', 'airport', 'acType', 'FlightID', 'acCat', 'origin','locid']
    df_traj=DF([], columns=cols_traj)
    
    df_noise=DF([], columns=['idx', 'ut', 'uid', 'locid', 'db', ])
    
    if test_type==0:
        
        nmout='train'
        
    else:

        nmout="test"
        
    out_traj_nm=nmout+'_trajectory_'+fl_nm[:-9]+".csv"
                 
    out_noise_nm=nmout+'_noise_'+fl_nm[:-9]+".csv"
    
    with open(dir_fl+fl_nm,'r') as fl:
    	rl=fl.readlines() 
#I have 2 different time series after prepariation I should merge them        
#the reason for
        #gets a line from lines
    for i, rline in enumerate(rl): 	

        rline=rline
        fields='{'+rline[rline.index('"t"'): ].split('{*}')[0].split("'")[0] 
        field_features=fields[:fields.index("nd")][:-4]+'}'

        field_resp=fields[fields.index("nd")+2:]
        field_resp='{'+'"'+field_resp[field_resp.index("ut"):-2] 
        acceptable_response=field_resp.replace("'", "\"")
        d2= json.loads(acceptable_response)
        #search how to append new key and value to a dictionary
        
        fields_meta=rline[7: rline.index('"t"')-2]+'}'
        acceptable_meta=fields_meta.replace("'", "\"")
        dict_meta= json.loads(acceptable_meta)
        #search how to append new key and value to a dictionary
        meta_fl=DF.from_dict(dict_meta, orient='index').T
        meta_fl['idx']=i
        meta_fl.set_index(['idx'], inplace=True, drop=False)
        locid=fields[fields.index("locid")+7:fields.index("locid")+10] 
        locid=int(locid)
        meta_fl['locid']=locid
        noise_fl=DF.from_dict(d2)
        
        json_acceptable_string = field_features.replace("'", "\"")
        d1= json.loads(json_acceptable_string)
        
        dict4df={your_key: d1[your_key] for your_key in cols[1:]}
        	#these two columns have different length cannot merge
        traj_fl=DF.from_dict(dict4df)
        traj_fl['idx']=i
        fly_id=int(rline[15:23])
        traj_fl.set_index(['idx'], inplace=True, drop=True)
        noise_fl['locid']=locid
        noise_fl['idx']=i        
        noise_fl['uid']=fly_id
        traj_fl=traj_fl.merge(meta_fl,on ='idx', how='left')
        traj_fl.reset_index(inplace=True, drop=False)
        df_traj=pd.concat([df_traj,traj_fl], axis=0)
        df_noise=pd.concat([df_noise,noise_fl], axis=0)
        
    df_traj[cols_traj].to_csv(out_traj_nm, index=0, encoding='utf8')
    df_noise.to_csv(out_noise_nm, index=0, encoding='utf8')

import glob

import os 

fl_nm_train='dca_pd_20160625_site_10_AO.njson'#"df_noise" "dca_pd_20160611_site_10_AO.njson" #first njoison odered by nameto train next 12 is test
fl_nm_test="dca_pd_20160626_site_10_AO.njson"

os.chdir("\10\")
for num, filejson in enumerate(sorted(glob.glob('*.njson'))):
    if (num-2)%3==0:
        test_type=1
    else:
        test_type=0
        
    cleaningJs(dir_fl='', fl_nm=filejson, test_type=test_type)
    
    cleaningJs(dir_fl='', fl_nm=filejson, test_type=test_type)
    
    
	
	


