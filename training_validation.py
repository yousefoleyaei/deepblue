#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 14:43:25 2019

@author: yousef
"""
# -*- coding: utf-8 -*-
"""
Spyder Editor
"""
import numpy as np
import tensorflow as tf
import pandas as pd
import random as rn
import matplotlib.pyplot as plt
import os

pd.options.display.width = 0
pd.set_option('display.expand_frame_repr', False)
np.random.seed(42)
rn.seed(12345)

session_conf=tf.ConfigProto(intra_op_parallelism_threads=8, inter_op_parallelism_threads=8)
from sklearn.preprocessing import StandardScaler
import keras
from keras.utils import to_categorical
from keras.models import Model
from keras.layers import Input, LSTM, Dense
from keras.optimizers import Adam
from keras import backend as K
sess=tf.Session(graph=tf.get_default_graph(), config=session_conf)

K.set_session(sess)

batch_size=20
epochs=100
latent_dim=200           
dropout = 0 
#    de_train[20*i:20*(1+i),1:,:]=arr_out[20*i:20*(1+i),:-1,:]


#arr_out=decoder_target_data
#dicto=max_len_train


def fun_dn_in(arr_out, dicto, val_idx):
    de_train=np.zeros(arr_out.shape)
    de_train[:,1:,:]=arr_out[:,:-1,:]
    de_train[:,0,:]=np.array([arr_out[j,dicto.flatten()[k]-1,:] for j, k in enumerate(val_idx)])
    return de_train

#def root_mean_squared_error(y_true, y_pred):
#        return (K.mean(K.square(y_pred - y_true)))

    
    
#def custom_mean_err(y_true, y_pred):
#    return K.mean(K.square(y_pred - y_true))
    
def train_generator1(mat_train, de_out_mat,max_len_train,j):
    
    fold_arr=np.arange(20,-1, -4)
    totalnon0=np.max(max_len_train, axis=1)#np.max(max_len, axis=1)
    while True:
        for i in np.arange(5):
            val_idx=np.arange(20*(1+i)-fold_arr[j],20*(1+i)-fold_arr[j+1])
            idx=list(set(np.arange(20*i,20*(1+i)))-set(val_idx))
            X=mat_train[idx,:totalnon0[i],:]
            y=de_out_mat[idx,:totalnon0[i],:]
            encoder_input_data=X
            decoder_target_data=y
            decoder_input_data=fun_dn_in(decoder_target_data, max_len_train, i, idx)    
            yield ({'input_1': encoder_input_data, 'input_2': decoder_input_data}, {'dense_1': decoder_target_data})
            
            
            

def validation_generator1(mat_train, de_out_mat,max_len_train, j):
    fold_arr=np.arange(20,-1, -4)
    totalnon0=np.max(max_len_train, axis=1)
                     
    while True:
        
        for i in np.arange(5):
            val_idx=np.arange(20*(1+i)-fold_arr[j],20*(1+i)-fold_arr[j+1])
            X_val=mat_train[val_idx,:totalnon0[i],:]
            y_val=de_out_mat[val_idx,:totalnon0[i],:]

            encoder_input_data=X_val
            decoder_target_data=y_val
            decoder_input_data=fun_dn_in(decoder_target_data, max_len_train, val_idx)    
            yield ({'input_1': encoder_input_data, 'input_2': decoder_input_data}, {'dense_1': decoder_target_data})



def train_generator(mat_train, de_out_mat, max_len_train):
    
    totalnon0=np.max(max_len_train, axis=1)
    
    while True:
        
        for i in np.arange(5):
            idx=np.arange(20*i, 20*(1+i))
            X_in=mat_train[idx,:totalnon0[i],:]
            y_out=de_out_mat[idx,:totalnon0[i],:]
            encoder_input_data=X_in
            decoder_target_data=y_out
            decoder_input_data=fun_dn_in(decoder_target_data, max_len_train, idx)    
            yield ({'input_1': encoder_input_data, 'input_2': decoder_input_data}, {'dense_1': decoder_target_data})



#hidden should look like this [layer=x, y] where x==y
# Define an input series and encode it with an LSTM.  added one categorical variabels
encoder_inputs = Input(shape=(None, 4))  #only 4 features
encoder = LSTM(latent_dim, dropout=dropout, return_state=True)
encoder_outputs, state_h, state_c = encoder(encoder_inputs)
encoder_states = [state_h, state_c]
decoder_inputs = Input(shape=(None, 1)) 
decoder_lstm = LSTM(latent_dim, dropout=dropout, return_sequences=True, return_state=True)
decoder_outputs, _, _ = decoder_lstm(decoder_inputs,
                                     initial_state=encoder_states)


decoder_dense = Dense(1) # 1 continuous output at each timestep
decoder_outputs = decoder_dense(decoder_outputs)

model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

model.summary()

model.compile(optimizer='sgd' , loss = 'mse', 
              metrics =["accuracy"]) #RMSE however see what is predicting it was 'mean_absolute_error' before Adam()

model.save_weights('wo_opType_mse_sga_cat_opType_noise_model.h5')


"""
 loss: String (name of objective function) or objective function.
            See [losses](/losses).
            If the model has multiple outputs, you can use a different loss
            on each output by passing a dictionary or a list of losses.
            The loss value that will be minimized by the model
            will then be the sum of all individual losses.
        metrics: List of metrics to be evaluated by the model
            during training and testing.
            Typically you will use `metrics=['accuracy']`.
            To specify different metrics for different outputs of a
            multi-output model, you could also pass a dictionary,
            such as `metrics={'output_a': 'accuracy'}`.
            
            
    """        
            




